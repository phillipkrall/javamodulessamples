package com.phillkrall.mod2;

import java.lang.reflect.Field;

public class Service2 {

	public static void main(String[] args) throws Exception {
		Field model = Class.forName("com.phillkrall.mod1.submod.Service1").getDeclaredField("model");
        Object newInstanceOfGear = Class.forName("com.phillkrall.mod1.submod.Service1").getDeclaredConstructor().newInstance();

        model.trySetAccessible();
        model.set(newInstanceOfGear, 2017);
        System.out.println(model.get(newInstanceOfGear));
	}
		
}
