# OpensModuleExample

This sample is just a simple example of what opens does for our modules.



## Setup
Import the OpensModuleExmple in Eclipse

## Explaination
If here we see that the keyword open is needed for our java module. This allows us to use reflection in our application. If we remove this keyword, then we will get an access error.

# 
```java
open module com.phillkrall.mod1 {
	exports com.phillkrall.mod1.submod;
}

```