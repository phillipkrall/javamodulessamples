# BestModuleApproach2

In this sample we demonstrate how we can have multiple module files under the sample Project and how they need to be imported into eclipse.



## Setup
In Eclipse import the following: 

*  import com.phillkrall.child.module
*  com.phillkrall.parent.mod
*  com.phillkrall.parent.mod2/com.phillkrall.parent.mod2.submod1
*  com.phillkrall.parent.mod2/com.phillkrall.parent.mod2.submod2

## Explaination
You shoud see eclipse complain about com.phillkrall.parent.mod. It is complaining that you have 2 module files in the same eclipse project. If you look at the parent.mod2, you will see that we have multiple modules in the parent folder, but have improted them as different eclipse projects, so it can build them correctly.

![](packageExplorer1.png?raw=true)

## Why would we want to us this?
This allows us to keep our parent module's submodule in the same repository.