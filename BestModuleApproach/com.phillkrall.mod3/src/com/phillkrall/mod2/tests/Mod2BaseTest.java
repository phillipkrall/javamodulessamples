package com.phillkrall.mod2.tests;

import java.lang.reflect.Method;
import java.util.Map;

import com.phillkrall.mod1.tests.BaseTest;

public class Mod2BaseTest extends BaseTest {

	public static void main(String[] args) {
		new Mod2BaseTest();
	}
	
	public Mod2BaseTest() {
		hello = "mod2";
		for(Method m: BaseTest.class.getMethods()) {
			System.out.println(m.getName());
		}
	}
	
	@Override
	public Map<String, Object> getData() {
		return null;
	}

}
