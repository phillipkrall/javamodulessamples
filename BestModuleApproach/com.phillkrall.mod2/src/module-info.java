/**
 * 
 */
/**
 * @author Phillip Krall
 *
 */
module com.phillkrall.mod2 {
	exports com.phillkrall.mod2.services;
	exports com.phillkrall.mod1.services;
	
	requires com.phillkrall.mod1;
}