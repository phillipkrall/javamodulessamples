/**
 * 
 */
/**
 * @author Phillip Krall
 *
 */
module com.phillkrall.mod1.tests {
	exports com.phillkrall.mod1.tests;
	
	requires com.phillkrall.mod1;
	requires junit;
}