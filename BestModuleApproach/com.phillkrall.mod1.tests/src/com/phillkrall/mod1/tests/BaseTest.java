package com.phillkrall.mod1.tests;

import java.util.Map;

public abstract class BaseTest {

	public abstract Map<String, Object> getData();

	protected String hello = "world";
}
