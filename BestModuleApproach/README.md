# BestModuleApproach

In this sample we demonstrate how we can have multiple module files in eclipse.


## Setup
In Eclipse import the following: 

- com.phillkrall.mod1
- com.phillkrall.mod1.tests
- com.phillkrall.mod2
- com.phillkrall.mod2.tests

## Explaination
Here we have 4 modules. 

- Mod1.tests depends on mod1, so we can use the modules from that in our tests. 
- Mod2 depends on mod1, so that we can utlize files from mod1 in our mod2 test.
- mod2.tests extends mod1 which gives access to everything from mod1.tests. This allows us to extend the bases from mod1.tests
- Mod2.tests cannot access anything from mod1 since it did not require mod1 in its module-info file.

![](packageExplorer1.png?raw=true)

## Why would we want to us this?
This would allow us to have multiple modules and test modules. This might mean that we need many repositories to faclitae these changes.